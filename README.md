# Auth function for username and passwrod authorization

## Commands
- ```npm install``` for installing all dependencies.
- ```npm run test``` for running tests.
- ```npm run coverage``` for seeing coverage along with running the tests.

## Auth function details
- The function takes 2 arguments.
- Password length should be greater than 8.
- Password should contain only alpha numeric and underscore.
- Password should start with an alphabet only.
- Returns ```username``` if correct otherwise returns ```false```.