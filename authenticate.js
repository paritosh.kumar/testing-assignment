module.exports = (username, password) => {
    // regex for password match.
    var passwordPattern=  /^[A-Za-z]\w{7,14}$/;
    return username && password && password.match(passwordPattern) ? username : false
}
