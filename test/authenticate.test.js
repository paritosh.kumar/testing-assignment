const assert = require('chai').assert
const expect = require('chai').expect
const authenticate = require('../authenticate')

describe('User auth testing',()=>{
    it('auth should return false when no username and password provided',()=>{
        assert.equal(authenticate(),false)
    })
    it('auth fail when no password',()=>{
        assert.equal(authenticate('username'),false)
    })
    it('password length smaller than 8 should fail',()=>{
        assert.equal(authenticate('username','1234567'),false)
    })
    it('password length greater than or equal to 8 should pass',()=>{
        let password = 'abce12345'
        assert.equal(authenticate('username',password),'username')
    })
    it('password  should contain alpha numeric and underscore only',()=>{
        let username = 'username'
        let correctPasswords = ['abce12345','asss2d_12','ds_123sdfg']
        correctPasswords.forEach(password=>assert.equal(authenticate(username,password),'username'))
    })
    it('auth should fail when incorrect password pattern provided',()=>{
        let username = 'username'
        let falsePasswords = ['ad12333--asd','fa//a//sd//123','asd2sss']
        falsePasswords.forEach(password=>assert.equal(authenticate(username,password),false))
    })
    it('password should start with alphabet only',()=>{
        let username = 'username'
        let correctPassword = 'ads123456'
        let falsePassword = '123asdadfasdf'
        assert.equal(authenticate(username,correctPassword),username)
        assert.equal(authenticate(username,falsePassword),false)
    })
    it('auth function should return same username',()=>{
        let username = 'tesla_dead'
        let password = 'abce12345'
        assert.equal(authenticate(username,password),username)
    })
})